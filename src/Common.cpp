#include "Common.h"

std::vector <glm::vec3> ofxRakau::getPlummerSphere(std::size_t nParticles,
                                         float clusterCoreRadius,
                                         float size,
                                         bool parallel){

    auto parts = rakau_common::get_plummer_sphere<float>(
        nParticles,
        clusterCoreRadius,
        size,
        parallel);

    vector <glm::vec3> positions;

    // and push the 
    for(size_t i = 0; i < nParticles; i++){
        positions.push_back(glm::vec3(parts.data()[i],
                                      parts.data()[i + nParticles],
                                      parts.data()[i + 2 * nParticles]));
    }

    return positions;
}
