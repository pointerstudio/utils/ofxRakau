#ifndef OFXRAKAU_COMMON_H
#define OFXRAKAU_COMMON_H

#include "ofMain.h"
#include "rakau/common.hpp"

namespace ofxRakau {
/**
 * @brief get a Plummer sphere
 *
 * https://en.wikipedia.org/wiki/Plummer_model
 * http://www.artcompsci.org/kali/vol/plummer/ch03.html
 *
 * This is a wrapper for a common function from rakau benchmark.
 * Thank you, Francisco!
 *
 * @param nParticles How many particles should there be?
 * @param clusterCoreRadius The radius of the most dense part.
 *                          Also called the plummer radius
 * @param size The total radius.
 * @param parallel Should we do fancy tbb parallel initialisation?
 *
 * @return A vector of particle positions.
 */
std::vector <glm::vec3> getPlummerSphere(std::size_t nParticles,
                                         float clusterCoreRadius,
                                         float size,
                                         bool parallel = true);

} // namespace ofxRakau

#endif
