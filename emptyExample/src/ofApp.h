#pragma once

#include <rakau/tree.hpp>
#include "common.hpp"
#include <array>

#include "ofMain.h"

using namespace rakau;
using namespace rakau::kwargs;
using namespace rakau_benchmark;

#define N_PARTICLES 12000

class ofApp : public ofBaseApp {
    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        octree <float> t;
        std::array <std::vector <float>, 3> accs;
        std::vector <glm::vec3> positions;

        ofEasyCam cam;
        ofLight light;
};
