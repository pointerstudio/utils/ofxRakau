#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    auto parts = get_plummer_sphere(size_t(N_PARTICLES), 1.0f, 0.0f, false);
    std::array <float, N_PARTICLES> init_masses;
    init_masses.fill(1.0f);

    // Create an octree from a set of particle coordinates and masses.
    t = octree <float>{x_coords = parts.data() + N_PARTICLES,
                       y_coords = parts.data() + 2 * N_PARTICLES,
                       z_coords = parts.data() + 3 * N_PARTICLES,
                       masses = init_masses.data(),
                       nparts = size_t(N_PARTICLES)};

    for(size_t i = 0; i < N_PARTICLES; i++){
        positions.push_back(glm::vec3(parts.data()[i],
                                      parts.data()[i + N_PARTICLES],
                                      parts.data()[i + 2 * N_PARTICLES]));
    }

    cam.enableMouseInput();

    ofEnableLighting();
    ofEnableDepthTest();
}

//--------------------------------------------------------------
void ofApp::update(){

    // Prepare output vectors for the accelerations.
    std::array <std::vector <float>, 3> accs;

    // Compute the accelerations with a theta parameter of 0.4.
    t.accs_u(accs, 0.4f);

    const auto bsize = t.box_size();
    const auto nparts = t.nparts();
    const float maximum_acc = FLT_MAX;

    t.update_particles_u([bs = bsize, np = nparts, &accs, &maximum_acc, this](const auto & p_its){
        auto x_it = p_its[0], y_it = p_its[1], z_it = p_its[2];
        for(std::remove_const_t <decltype(np)> i = 0; i < np; ++i){
            //x_it[i] += min(accs[0][i]);
            //y_it[i] += min(accs[1][i]);
            //z_it[i] += min(accs[2][i]);
            x_it[i] += max(-1 * maximum_acc, min(maximum_acc, accs[0][i]));
            y_it[i] += max(-1 * maximum_acc, min(maximum_acc, accs[1][i]));
            z_it[i] += max(-1 * maximum_acc, min(maximum_acc, accs[2][i]));
            positions[i].x = x_it[i];
            positions[i].y = y_it[i];
            positions[i].z = z_it[i];
        }
    });

    //std::cout << "FRAME///" << endl;
    //std::cout << ofToString(t) << endl;
    //std::cout << "///FRAME" << endl;

    light.setPosition(100, 0, 0);
    light.enable();
}

//--------------------------------------------------------------
void ofApp::draw(){
    std::stringstream sts;
    sts << ofGetFrameRate();
    ofDrawBitmapString(sts.str(), 20, 20);

    cam.begin();
    for(auto & p : positions){
        ofSetColor(ofColor::white);
        ofDrawSphere(p, 1);
    }
    cam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
