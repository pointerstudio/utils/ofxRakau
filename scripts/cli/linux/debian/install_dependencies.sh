#!/usr/bin/env bash

if [ $EUID == 0 ]; then
	echo "this script should not be run as root"
	echo "if you want to do it anyway, just get rid of this conditional"
	echo "usage:"
	echo $0
	exit $exit_code
   exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TMP_DIR=$DIR/tmp/ofxRakau
INSTALL_DIR=/usr/local

DEBIAN_CODENAME=$(cat /etc/os-release | grep VERSION_CODENAME= | sed "s/VERSION_CODENAME\=//")
NVIDIA_DRIVER_VERSION=$(apt-cache policy nvidia-driver | grep 'Installed:' | cut -c 14-)
NVIDIA_FROM_BACKPORTS="false"
APT_SOURCE=""

echo "detecting if nvidia-driver is installed via buster-backports"
if [ ${#NVIDIA_DRIVER_VERSION} -gt 0 ]; then
    # check if the installed version is from buster-backports
    if [ $(apt-cache showpkg nvidia-driver \
            | grep $NVIDIA_DRIVER_VERSION \
            | grep $DEBIAN_CODENAME-backports \
            | wc -l) -gt 0 ]; then
        NVIDIA_FROM_BACKPORTS="true"
        APT_SOURCE="-t $DEBIAN_CODENAME-backports"
    fi
fi

echo "your cmake version in apt:"
apt-cache show cmake | grep "Version:" | cut -c 10-
echo "is this bigger than minimum version of rakau and xsimd?\
	at the moment of writing this is 3.3.0, check CmakeLists.txt"

read -n1 -p "go on and install cmake? [y,n]" doit
case $doit in
  y|Y) sudo apt install cmake ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

read -n1 -p "go on and install checkinstall? [y,n]" doit
case $doit in
  y|Y) sudo apt install checkinstall ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

INSTALL_XSIMD=False
read -n1 -p "go on and install xsimd? [y,n]" doit
case $doit in
  y|Y) INSTALL_XSIMD=True ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

if [ $INSTALL_XSIMD == True ]; then
	mkdir -p $TMP_DIR
	cd $TMP_DIR
	git clone --depth 1 --branch 7.5.0 https://github.com/xtensor-stack/xsimd
	rm -rf xsimd_build
	mkdir xsimd_build
	cd xsimd_build
	
	cmake -D CMAKE_INSTALL_PREFIX=$INSTALL_DIR $TMP_DIR/xsimd
	make
	
	echo ""
	echo "you will now be asked some questions by checkinstall"
	echo "this is so you can later easily remove xsimd"
	echo "just press enter"
	echo ""
	read -p ok
	
	sudo checkinstall --pkgname=xsimd
fi

read -n1 -p "go on and install $APT_SOURCE cuda? [y,n]" doit
case $doit in
  y|Y) sudo apt install $APT_SOURCE nvidia-cuda-dev nvidia-cuda-toolkit ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

echo "known bug, if we have buster, and not nvidia-driver via backports, following will fail \n
fix it then by manually replacing \$APT_SOURCE with \"-t buster-backports\" in next line"
read -n1 -p "otherwise.. go on and install libboost1.74-all-dev? [y,n]" doit
case $doit in
  y|Y) sudo apt install $APT_SOURCE libboost1.74-all-dev ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

INSTALL_RAKAU=False
read -n1 -p "go on and install rakau? [y,n]" doit
case $doit in
  y|Y) INSTALL_RAKAU=True ;;
  n|N) echo "okay, then not" ;;
  *) echo dont know ;;
esac

if [ $INSTALL_RAKAU == True ]; then
	mkdir -p $TMP_DIR
	cd $TMP_DIR
	git clone --depth 1 --branch "v0.3" https://github.com/bluescarni/rakau
	rm -rf rakau_build
	mkdir rakau_build
	cd rakau_build
	
	cmake -DRAKAU_WITH_CUDA=1 -DRAKAU_BUILD_BENCHMARKS=1 -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR $TMP_DIR/rakau
	make
	
	echo ""
	echo "you will now be asked some questions by checkinstall"
	echo "this is so you can later easily remove rakau"
	echo "just press enter"
	echo ""
	read -p ok
	sudo checkinstall --pkgname=rakau
fi

echo "DONE"
